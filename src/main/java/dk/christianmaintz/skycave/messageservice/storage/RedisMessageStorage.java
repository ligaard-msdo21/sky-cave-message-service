package dk.christianmaintz.skycave.messageservice.storage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.christianmaintz.skycave.messageservice.domain.MessageRecord;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

//This class is used if active spring profile is redis
@Profile("redis")
@Component
@RequiredArgsConstructor
public class RedisMessageStorage implements MessageStorage {

    private final String MESSAGE_PREFIX = "message:";

    private final JedisPool jedisPool;

    private final ObjectMapper objectMapper;

    @Override
    public MessageRecord addMessage(String positionString, MessageRecord messageRecord) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();

            messageRecord.setCreatorTimeStampISO8601(ZonedDateTime.now());
            messageRecord.setId(UUID.randomUUID().toString());
            String asJSON = objectMapper.writeValueAsString(messageRecord);
            jedis.hset(MESSAGE_PREFIX + positionString, messageRecord.getId(), asJSON);
            return messageRecord;
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
    }

    @Override
    public MessageRecord updateMessage(String positionString, String messageId, MessageRecord newMessageRecord) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();

            String originalMessageAsString = jedis.hget(MESSAGE_PREFIX + positionString, messageId);
            MessageRecord originalMessage = objectMapper.readValue(originalMessageAsString, MessageRecord.class);
            // Bail out if no message with given id exists
            if (originalMessage == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            // Bail out if the found message was not created by same person
            if (! originalMessage.getCreatorId().equals(newMessageRecord.getCreatorId())) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            originalMessage.setContents(newMessageRecord.getContents());
            String asJSON = objectMapper.writeValueAsString(originalMessage);
            jedis.hset(MESSAGE_PREFIX + positionString, messageId, asJSON);
            return originalMessage;
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
    }

    @Override
    public List<MessageRecord> getMessageList(String positionString, int startIndex, int pageSize) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();

            Map<String, String> messageMap = jedis.hgetAll(MESSAGE_PREFIX + positionString);
            if (messageMap == null) return null;
            Object[] array = messageMap.keySet().toArray();
            int size = array.length;
            int firstIndex = startIndex * pageSize > size ? size : startIndex * pageSize;
            int lastIndex = firstIndex + pageSize > size ? size : firstIndex + pageSize;
            List<MessageRecord> records = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Object key = array[i];
                records.add(objectMapper.readValue(messageMap.get(key), MessageRecord.class));
            }
            records.sort(Comparator.comparing(MessageRecord::getCreatorTimeStampISO8601).reversed());
            return records.subList(firstIndex, lastIndex);
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
    }

    @Override
    public MessageRecord getMessage(String positionString, String messageId) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();

            Map<String, String> messageMap = jedis.hgetAll(MESSAGE_PREFIX + positionString);
            if (messageMap == null) return null;
            Object[] array = messageMap.keySet().toArray();
            int size = array.length;
            List<MessageRecord> records = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                Object key = array[i];
                records.add(objectMapper.readValue(messageMap.get(key), MessageRecord.class));
            }
            Optional<MessageRecord> result = records.stream().filter(messageRecord -> messageRecord.getId().equals(messageId)).findFirst();
            if (result.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            return result.get();
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
    }
}
