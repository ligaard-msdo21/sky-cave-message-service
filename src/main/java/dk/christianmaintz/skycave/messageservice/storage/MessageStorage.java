package dk.christianmaintz.skycave.messageservice.storage;

import dk.christianmaintz.skycave.messageservice.domain.MessageRecord;

import java.util.List;

public interface MessageStorage {
    /**
     * Add a message to the wall of specific room. Messages are to be ordered chronological in the storage, with a
     * notion of 'newest first', that is, the message is not appended as the last one to a list, rather like pushed as
     * the top one on a stack.
     *
     * @param positionString the position of the room in the cave
     * @param messageRecord  the message to add to the 'wall'
     */
    MessageRecord addMessage(String positionString, MessageRecord messageRecord);

    /**
     * Update a wall message for a given position in the cave with the given id. Only the contents of 'newMessageRecord'
     * is used to overwrite the contents of the original message, if it is valid.
     *
     * @param positionString   the (x,y,z) position in the cave of the room that contains the message to update
     * @param messageId        the unique id of the message to update
     * @param newMessageRecord a messagerecord that must include the playerID of the player wanting to change the wall
     *                         message, as well as the new contents.
     * @return status code for whether it worked out or not, reusing the HTTP codes: 200 OK, 404 NOT FOUND if the
     * messageId was not found for any messages in this room; 400 UNAUTHORIZED if the message was not created by this
     * player.
     */
    MessageRecord updateMessage(String positionString, String messageId, MessageRecord newMessageRecord);

    /**
     * Return a page of the messages in a given room. A page has a given size and it starts at the given startIndex. The
     * startIndex == 0 is the NEWEST message on the wall in the room. That is: addMessage("1"), addMessage("2"),
     * followed by getMessageList(0, 16) = get page 0, should return ["2", "1"]
     *
     * @param positionString the position of the room in the cave
     * @param startIndex     absolute index of message to start the page
     * @param pageSize       getNumber of messages to retrieve
     * @return list of messages starting at index 'startIndex' and of maximal length 'pageSize'. If no messages are
     * available on the page, return an empty list (not a null list).
     */
    List<MessageRecord> getMessageList(String positionString, int startIndex, int pageSize);

    MessageRecord getMessage(String positionString, String messageId);
}
