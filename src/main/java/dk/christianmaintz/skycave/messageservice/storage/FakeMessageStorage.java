package dk.christianmaintz.skycave.messageservice.storage;

import dk.christianmaintz.skycave.messageservice.domain.MessageRecord;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

//This class is used if active spring profile is fake
@Profile("fake")
@Component
public class FakeMessageStorage implements MessageStorage {

    private final ConcurrentHashMap<String, List<MessageRecord>> messageMap = new ConcurrentHashMap<>();

    @Override
    public MessageRecord addMessage(String positionString, MessageRecord messageRecord) {
        List<MessageRecord> msgInThisRoom = getMessageMapForRoom(positionString);
        // Simulate 'classic DB' behaviour, assign unique
        // id to item and timestamp it
        MessageRecord newRecord = new MessageRecord(messageRecord);
        newRecord.setCreatorTimeStampISO8601(ZonedDateTime.now());
        newRecord.setId(UUID.randomUUID().toString());

        // Append to position 0, so older records are pushed towards the end
        msgInThisRoom.add(0, newRecord);
        messageMap.put(positionString, msgInThisRoom);
        return newRecord;
    }

    @Override
    public MessageRecord updateMessage(String positionString, String messageId, MessageRecord newMessageRecord) {
        List<MessageRecord> msgInThisRoom = getMessageMapForRoom(positionString);

        // Search and find index of first message with same ID as updating message
        OptionalInt indexOpt = IntStream.range(0, msgInThisRoom.size())
                .filter(i -> messageId.equals(msgInThisRoom.get(i).getId()))
                .findFirst();

        // Bail out if no message with given id exists
        if (!indexOpt.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        MessageRecord match = msgInThisRoom.get(indexOpt.getAsInt());

        // Bail out if the found message was not created by same person
        if (!match.getCreatorId().equals(newMessageRecord.getCreatorId())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        // Update message and enter it back into the list
        MessageRecord updatedOne = new MessageRecord(match);
        updatedOne.setContents(newMessageRecord.getContents());
        msgInThisRoom.set(indexOpt.getAsInt(), updatedOne);

        return updatedOne;
    }

    @Override
    public List<MessageRecord> getMessageList(String positionString, int startIndex, int pageSize) {
        List<MessageRecord> msgInThisRoom = getMessageMapForRoom(positionString);
        int size = msgInThisRoom.size();
        int firstIndex = startIndex * pageSize > size ? size : startIndex * pageSize;
        int lastIndex = firstIndex + pageSize > size ? size : firstIndex + pageSize;
        return msgInThisRoom.subList(firstIndex, lastIndex);
    }

    @Override
    public MessageRecord getMessage(String positionString, String messageId) {

        List<MessageRecord> msgInThisRoom = getMessageMapForRoom(positionString);

        // Search and find index of first message with same ID as updating message
        OptionalInt indexOpt = IntStream.range(0, msgInThisRoom.size())
                .filter(i -> messageId.equals(msgInThisRoom.get(i).getId()))
                .findFirst();

        // Bail out if no message with given id exists
        if (!indexOpt.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return msgInThisRoom.get(indexOpt.getAsInt());
    }

    public List<MessageRecord> getMessageMapForRoom(String positionInCave) {
        List<MessageRecord> msgInThisRoom;
        msgInThisRoom = messageMap.get(positionInCave);
        if (msgInThisRoom == null) {
            msgInThisRoom = new ArrayList<MessageRecord>();
        }
        return msgInThisRoom;
    }
}
