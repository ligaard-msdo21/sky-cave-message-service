package dk.christianmaintz.skycave.messageservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkyCaveMessageServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkyCaveMessageServiceApplication.class, args);
	}

}
