package dk.christianmaintz.skycave.messageservice.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("health")
public class HealthController {

    private final JedisPool jedisPool;
    private final ApplicationAvailability applicationAvailability;

    @Value("${pom.version}")
    private String pomVersion;

    @Value("${spring.profiles.active}")
    private String activeProfile;


    @GetMapping
    public HealthState getHealth() {

        HealthState healthState = new HealthState();
        //Host ip address/addresses
        healthState.setIps(getAllIps());

        //version number of JVM
        healthState.setJvmVersion(System.getProperty("java.version"));

        //application version or commit ID
        healthState.setVersion(pomVersion);

        //Whether the instance is accepting work
        healthState.setReady(applicationAvailability.getReadinessState() == ReadinessState.ACCEPTING_TRAFFIC);

        healthState.setRedisReady(getRedisStatus());

        return healthState;
    }

    private List<String> getAllIps() {
        List<String> ips = new ArrayList<>();
        Enumeration<NetworkInterface> e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        while(e.hasMoreElements())
        {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements())
            {
                InetAddress i = (InetAddress) ee.nextElement();
                // Filter out ipv6 addresses
                String ip = i.getHostAddress();
                if ( ! ip.contains("%"))
                    ips.add(ip);
            }
        }
        return ips;
    }

    private boolean getRedisStatus() {
        if (activeProfile.equals("fake")) {
            return true;
        }
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.ping().equals("PONG");
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
    }

    @Data
    private class HealthState {
        private List<String> ips;
        private boolean ready;
        private String version;
        private String jvmVersion;
        private boolean redisReady;
    }
}
