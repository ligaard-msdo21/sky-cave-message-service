package dk.christianmaintz.skycave.messageservice.controller.v1;

import dk.christianmaintz.skycave.messageservice.domain.MessageRecord;
import dk.christianmaintz.skycave.messageservice.storage.MessageStorage;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@RestController
@RequestMapping("skycave/v1/messages")
public class MessageServiceControllerV1 {

    private final MessageStorage messageStorage;

    private final Pattern positionStringPattern = Pattern.compile("\\(\\d,\\d,\\d\\)");

    @GetMapping("{positionString}")
    public ResponseEntity<List<MessageRecord>> getMessageList(@PathVariable("positionString") String positionString,
                                              @RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,
                                              @RequestParam(value = "pageSize", defaultValue = "8") Integer pageSize) {
        validatePositionString(positionString);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<MessageRecord> messageRecords = messageStorage.getMessageList(positionString, pageNumber, pageSize);
        if (CollectionUtils.isEmpty(messageRecords)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ArrayList<>());
        }
        return ResponseEntity.ok(messageRecords);
    }

    @GetMapping("{positionString}/{messageId}")
    public MessageRecord getMessage(@PathVariable("positionString") String positionString,
                                    @PathVariable("messageId") String messageId) {
        validatePositionString(positionString);
        return messageStorage.getMessage(positionString, messageId);
    }

    @PostMapping("{positionString}")
    public ResponseEntity<MessageRecord> addMessage(@PathVariable("positionString") String positionString,
                                                   @RequestBody MessageRecord messageRecord,
                                                   UriComponentsBuilder uriComponentsBuilder) {
        validatePositionString(positionString);
        validateMessageRecord(messageRecord);

        MessageRecord createdMessage = messageStorage.addMessage(positionString, messageRecord);
        UriComponents uriComponents = uriComponentsBuilder.path("/skycave/v1/messages/{positionString}/{messageId}").buildAndExpand(positionString, createdMessage.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(createdMessage);
    }

    @PutMapping("{positionString}/{messageId}")
    public MessageRecord updateMessage(@PathVariable("positionString") String positionString,
                                       @PathVariable("messageId") String messageId,
                                       @RequestBody MessageRecord messageRecord) {
        validatePositionString(positionString);
        validateMessageRecord(messageRecord);
        return messageStorage.updateMessage(positionString, messageId, messageRecord);
    }

    private void validatePositionString(String positionString) {
        Matcher matcher = positionStringPattern.matcher(positionString);

        if (!matcher.find()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "positionString format is not correct");
        }
    }

    private void validateMessageRecord(MessageRecord messageRecord) {

        if (StringUtils.isEmpty(messageRecord.getContents())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "message contents are missing");
        } else if (StringUtils.isEmpty(messageRecord.getCreatorId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "message creatorId is missing");
        } else if (StringUtils.isEmpty(messageRecord.getCreatorName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "message creatorMame is missing");
        }
    }
}
