# Dockerfile

# Using image that has support for maven
FROM maven:3.6.1-jdk-11 AS builder

# Author of this file
LABEL maintainer="Student:Christian;Group:Bravo"

# sets the work directory
WORKDIR /root/message

# copies all relevant folders and files
COPY /src ./src
COPY /pom.xml ./pom.xml

# Creating the jar file
RUN mvn package -DskipTests

# Using openjdk slim image for running service
FROM openjdk:11-jre-slim

RUN apt-get update && apt-get install -y curl

# copy jar file from builder
COPY --from=builder /root/message/target/sky-cave-message-service-1.1.0.jar /root/message/sky-cave-message-service-1.1.0.jar

# Exposing the 7777 port, remember to hook it up when creating your container
EXPOSE 5777

# This uses the default http.cpf gradle configuration when the container is started
CMD java -jar -Dspring.profiles.active=fake /root/message/sky-cave-message-service-1.1.0.jar

#TODO below, and also add pull description to fix issues
# Usage:
# step 1: Move this file in your code repository root folder
# step 2: Build the image using > docker build -f ./Dockerfile-multistage -t dockerfile-multistage .
# step 3: Create a container with > docker run -d -p 7777:7777 --name mydaemon dockerfile-multistage
# optionally you can assign another cpf file running > docker run -d -p 7777:7777 --name mydaemon dockerfile-multistage java -jar /root/cave/daemon.jar [cpf file name(i.e. dummy.cpf)]